﻿using System.Configuration;
using System.Data.SqlClient;

namespace DbInstaller
{

    public class Program
    {
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["AdoNetPractice"].ToString();
        private static readonly string MasterConnectionString = ConfigurationManager.ConnectionStrings["Master"].ToString();

        static void Main(string[] args)
        {
            if (!DbExist())
            {
                ExecuteSql(MasterConnectionString, createDBSql);
            }
            ExecuteSql(ConnectionString, createTablesSql);
        }

        private static bool DbExist()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void ExecuteSql(string connectionString, string sqlCommand)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (var command = new SqlCommand(sqlCommand, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        #region Sql

        private static string createDBSql = $@"CREATE DATABASE [AdoNetPractice]";

        private static string createTablesSql = $@"
CREATE TABLE dbo.Clients
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(150) NOT NULL, 
    [Email] NVARCHAR(150) NOT NULL, 
    [Phone] NVARCHAR(150) NOT NULL
);
CREATE TABLE [dbo].[Products]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(150) NOT NULL, 
    [Price] DECIMAL(18,2) NOT NULL
);
CREATE TABLE [dbo].[Orders] (
    [Id]        INT IDENTITY (1, 1) NOT NULL,
    [ProductId] INT NOT NULL,
    [ClientId]  INT NOT NULL,
    [Count]     INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Orders_ToProducts] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products] ([Id]),
    CONSTRAINT [FK_Orders_ToClients] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Clients] ([Id])
);
";
#endregion
    }
}
