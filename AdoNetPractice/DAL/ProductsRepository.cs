﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace AdoNetPractice.DAL
{
    internal class ProductsRepository : DbConnection, IRepository<Product>
    {
        public List<Product> List()
        {
            var products = new List<Product>();

            const string sqlExpression = "SELECT * FROM dbo.Products";
            using (var command = new SqlCommand(sqlExpression, this.Connection))
            {
                var reader = command.ExecuteReader();

                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read()) // построчно считываем данные
                    {
                        products.Add(new Product
                        {
                            Id = (int)reader["id"],
                            Name = (string)reader["name"],
                            Price = (decimal)reader["price"]
                        });
                    }
                }
                reader.Close();
            }
            return products;
        }

        public void Insert(Product product)
        {
            const string sqlExpression = "INSERT INTO dbo.Products (Name, Price) VALUES (@Name, @Price); SELECT SCOPE_IDENTITY()";

            using (var command = new SqlCommand(sqlExpression, this.Connection))
            {
                // определяем параметры и их значение
                command.Parameters.Add("@Name", SqlDbType.NVarChar, 50).Value = product.Name;
                command.Parameters.Add("@Price", SqlDbType.Decimal).Value = product.Price;

                //получаем Id новой строки, если необходимо
                var identity = command.ExecuteScalar();
            }
        }

        public void Update(Product product)
        {
            const string sqlExpression = "UPDATE dbo.Products SET Name=@Name, Price=@Price WHERE Id=@Id";

            using (var command = new SqlCommand(sqlExpression, this.Connection))
            {
                // определяем параметры и их значение
                command.Parameters.Add("@Id", SqlDbType.Int).Value = product.Id;
                command.Parameters.Add("@Name", SqlDbType.NVarChar, 50).Value = product.Name;
                command.Parameters.Add("@Price", SqlDbType.Decimal).Value = product.Price;

                int affectedRowsCount = command.ExecuteNonQuery();
            }
        }

        public void Delete(int productId)
        {
            const string sqlExpression = "DELETE FROM dbo.Products WHERE Id=@Id";
            using (var command = new SqlCommand(sqlExpression, this.Connection))
            {
                command.Parameters.Add("@Id", SqlDbType.Int).Value = productId;

                int affectedRowsCount = command.ExecuteNonQuery();
            }
        }
    }
}